<?php
  include('admin/runtime.php');
  include('admin/lang.php');
  perch_layout('base.top', array(
    'id'=>'contact',
    'class'=>'info',
  ));
  perch_layout('base.header');
?>

<div class="wrapper content">

  <?php perch_content('Contact - '.$lang); ?>

</div>

<?php perch_layout('base.bottom'); ?>
