## Cheatsheet

### Markdown

[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)



### Bilddimensionen
bei 72ppi

1920px x [Hoehenverhaeltnis Querformat]

[Weitenverhaeltnis Hochformat] x 1920px

oder einfach: die laengsten Seiten <= 1920px

### Zeilenumbruch “forcieren”

HTML Element `<br>` verwenden.
(Dies ist insbesondere fuer die Betitelung auf der Startseite gedacht.)

### Zweisprachige Attribut- und Beschrieb Felder

Die Logik:
WENN
man sich auf der Englischen Seite befindet
UND
existiert ein Eintrag im
entsprechenden englischsprachigen Feld
DANN
wird dieses Feld angezeigt.
SONST
wird das deutschsprachige Feld angegzeigt. (Dieses dient eigentlich weniger als
sprachbezogenes Feld, sondern eher als "Default"-Feld.)

WENN
man sich auf der Deutschen Seite befindet
DANN
wird das "Default"-Feld angzeigt.

### Ausrichtung der Thumbnails auf der Startseite

Für das Feld "Size":
[CSS Background-Size Property](https://developer.mozilla.org/de/docs/Web/CSS/background-size)

Für das Feld "Position":
[CSS Background-Position Property](https://developer.mozilla.org/de/docs/Web/CSS/background-position)

### GIFS als Thumnails für Video Einträge

Die Logik:
WENN
ein Video Eintrag existiert
DANN
wird das hochgeladene GIF animiert angezeigt.
SONST
werden animierte GIFs statisch angezeigt, bzw. das erste Frame des GIFs.
