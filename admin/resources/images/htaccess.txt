<IfModule mod_rewrite.c>
  RewriteEngine on
  RewriteCond %{REQUEST_URI} !watermark.php

  # add perch:content color mod to image url and
  # pass it as param to php script. in the script
  # get that param to switch between colors.

  RewriteRule ^(.+)\.(png|jpg|jpe|jpeg|gif|bmp) watermark.php?file=$1\.$2 [QSA,NC]
</IfModule>
