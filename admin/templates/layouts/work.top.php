<!doctype html>
<html class="no-js" lang="de">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <base href="/">

    <title>froelicher | bietenhader | <?php perch_pages_navigation_text(); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="favicon.png">

    <link rel="stylesheet" href="css/frobi-min.css">
    <link rel="stylesheet" href="css/gallery-min.css">

    <!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script>window.html5 || document.write('<script src="js/dep/html5shiv.js"><\/script>')</script>
    <![endif]-->
  </head>
  <body id="<?php perch_layout_var('id'); ?>" class="<?php perch_layout_var('class'); ?>">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
