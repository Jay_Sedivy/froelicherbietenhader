    <!-- TODO: Overlaps gallery media. Does not look good. Also
    This has to be outside the header because iOS 9 Safari sux.
    https://remysharp.com/2012/05/24/issues-with-position-fixed-scrolling-on-ios
    <div class="header-main__logo">
      <h1>frölicher | bietenhader</h1>
    </div> -->
    <header class="header-main js-hide ini-hide">
      <div class="grid">
        <nav class="grid__item header-main__nav nav">
          <?php
            perch_pages_navigation(array(
            // 'from-path'            => '/',
            // 'levels'               => 0,
            'hide-extensions'      => true,
            // 'hide-default-doc'     => false,
            // 'flat'                 => false,
            'template'             => 'nav_item.'.$_SESSION['lang'].'.html',
            // 'include-parent'       => false,
            // 'skip-template'        => false,
            // 'siblings'             => false,
            // 'only-expand-selected' => false,
            // 'add-trailing-slash'   => false,
            // 'navgroup'             => false,
            // 'include-hidden'       => true,
            ));
          ?>
        </nav>
        <div class="grid__item header-main__toggle-btn btn">
          <div class="three-line-icn icn">
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
    </header>

    <main role="main">
