<?php
  session_start();
  $lang = 'de'; // default lang
  switch(perch_get('lang')) {
    case 'en':
      $lang = 'en';
      break;
    case 'de':
      $lang = 'de';
      break;
    // add more cases for language options here...

    default:
      if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
      }
      break;
  }
  $_SESSION['lang'] = $lang;
