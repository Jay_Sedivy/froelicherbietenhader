<?php
    define('PERCH_LICENSE_KEY', 'P21305-DMK023-GAF902-KFW400-CPT014');

    define("PERCH_DB_USERNAME", 'root');
    define("PERCH_DB_PASSWORD", '');
    define("PERCH_DB_SERVER", "froelicherbietenhader.s");
    define("PERCH_DB_DATABASE", "frobi");
    define("PERCH_DB_PREFIX", "perch2_");

    define('PERCH_TZ', 'UTC');

    define('PERCH_EMAIL_FROM', 'jerome@shtill.com');
    define('PERCH_EMAIL_FROM_NAME', 'Jerome Imfeld');

    define('PERCH_LOGINPATH', '/admin');
    define('PERCH_PATH', str_replace(DIRECTORY_SEPARATOR.'config', '', __DIR__));
    define('PERCH_CORE', PERCH_PATH.DIRECTORY_SEPARATOR.'core');

    define('PERCH_RESFILEPATH', PERCH_PATH . DIRECTORY_SEPARATOR . 'resources');
    define('PERCH_RESPATH', PERCH_LOGINPATH . '/resources');

    define('PERCH_HTML5', true);
