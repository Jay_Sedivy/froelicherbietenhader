+(function(window, document, $) {

  var
    $window = $(window),
    $main = $('main'),
    $work_header_first = $('.work__header').first(),
    scroll_adjust_val = $work_header_first[0].offsetTop - 250;

  // Position scrolling body
  if (window.storage) {
    var
      x = localStorage.getItem('x') ?
        localStorage.getItem('x') : 0,
      y = localStorage.getItem('y') ?
        localStorage.getItem('y') : scroll_adjust_val;

    $main.scrollLeft(x);
    $window.scrollTop(y);

    localStorage.removeItem('x');
    localStorage.removeItem('y');

  } else {
    $window.scrollTop(scroll_adjust_val);
  }

  if (window.storage) {
    $('[data-work-index], .work__header').click(function () {
      localStorage.setItem('x', $main.scrollLeft());
      localStorage.setItem('y', $window.scrollTop());
    });
  }

  /* Append indices to image links in each .work.
   *
   * NOTE: Unfortunately, i do not know how i could append indices with the
   * template language. There are two repeaters in the template, one
   * for the images in the top part of '.work' and one for the bottom
   * part. perch_content id=perch_item_index does not work beyond
   * a repeater, which makes sense. I just could not find a better html
   * structur as using a top and and a bottom container each with a
   * repeater for convenient editing. */

  // '.work--top' simply provides the count of works on the page.
  $('.work--top').each(function (i) {
    $('[data-work-index="'+ ++i +'"]').each(function (j, x) {
      x.href += '/'+ ++j;
    });
  });


  // var arr = [0];
  // $window.on('mousewheel', function(e) {
  //   e.preventDefault();
  //   var scrolled = true;
  //   var t;
  //
  //   $window.scrollTop($window.scrollTop()-e.deltaY);
  //   arr[arr.length] = e.deltaY;
  //   if (arr[arr.length-1] > arr[arr.length-10]) {
  //     clearTimeout(t);
  //     t = setTimeout(function() {
  //       console.log('adjust');
  //     }, 100);
  //   }
    // if (e.deltaY < -70) {
    //
    //   if (scrolled) {
    //     scrolled = false;
    //     $('body, html').stop().animate({
    //       scrollTop: '+=170'
    //     }, 800, 'swing', function() {
    //       scrolled = true;
    //     });
    //   }
    // }
    //
    // if (e.deltaY > 20) {
    //   $window.scrollTop($window.scrollTop() - 170);
    // }
  // });


  $(document).ready(function() {

    // TODO: Threshold is rather (too) large. Lazyloading does not
    // work seamlessly with background-images and/or vertical scroll
    // pages using small thresholds. Maybe it has something to do
    // with simultaneous loading of the placeholder. (???)
    $('a.lazy').lazyload({
      container: $('main'),
      threshold : 800,
      failure_limit: 100
    });
  });
})(window, document, jQuery);
