+(function(window, document, $) {

  var
    $window = $(window),
    $main = $('main'),
    page = document.body.id,
    $header_main = $('.header-main'),
    $header_main_nav = $header_main.find('.header-main__nav'),
    $header_main_toggle_btn = $header_main.find('.header-main__toggle-btn'),
    window_width = $window.width();
    curScrollPos = 0;
    BREAKPOINT_LARGE = 800;

  /*
   * Used by: $header_main_toggle_btn.[click|on('click')]
   *
   * Index and 'info' pages:
   *   Initially, $header_main is visible and
   *   $header_main_toggle_btn is hidden.
   * Work details page:
   *   Initially, $header_main is hidden.
   *   $header_main_toggle_btn is visible. */
  var toggleHeader = function () {
    $header_main
      .toggleClass('js-hide');
  };

  /*
   * Used by: $window.scroll
   *
   * When the user scrolls a certain amount to the
   * the right, hide the header to provide more space
   * for viewing the work. */
  var
    setScrollPos = true,
    curScrollPos = 0;

  var toggleHeaderByScrollPos = function (e) {

    if (window_width < BREAKPOINT_LARGE) return;

    var
      scrollleft = $main.scrollLeft(),
      $header = $header_main;

    curScrollPos = setScrollPos ? scrollleft : curScrollPos;
    setScrollPos = false;

    if (scrollleft > (curScrollPos + 32)) {
      $header.addClass('js-hide byscroll');
      setScrollPos = true;
    } else if (scrollleft < (curScrollPos - 32)) {
      $header.removeClass('js-hide byscroll');
      setScrollPos = true;
    }
  };


  var toggleHeaderByResize = function() {
    window_width = $window.width();
    $header = $header_main;
    if (!$header.hasClass('byscroll')) {
      if (window_width < BREAKPOINT_LARGE) {
        $header.addClass('js-hide');
      } else if (window_width >= BREAKPOINT_LARGE) {
        $header.removeClass('js-hide');
      }
    }
  };


  /*
   * Handler */
  $header_main_toggle_btn.click(toggleHeader);
  switch (page) {
    case 'work':
      break;
    default:
      setTimeout(function () {
        $header_main.removeClass('ini-hide');
      }, 200);
      if (window_width < BREAKPOINT_LARGE) $header_main.addClass('js-hide');
      $main.scroll(toggleHeaderByScrollPos);
      $window.resize(toggleHeaderByResize);
  }
})(window, document, jQuery);
