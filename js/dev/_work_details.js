+(function(window, document, $) {

  var touch = (function() {
    return 'ontouchstart' in window  ||       // works on most browsers
            navigator.maxTouchPoints;       // works on IE10/11 and Surface
  })();

  var fox = (function() {
    return navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
  })();

  // TODO: Not everything has to be in the ready function!!
  $(document).ready(function() {
    var config = {
      fade_audio: false,
      fade_in_audio_duration: 4000,
      fade_out_audio_duration: 2000
    };

    var
      $window = $(window),
      $body = $('body'),
      $scrollBody = $body.add('html'),
      $hustensaft = (touch || !fox) ? $window : $scrollBody,
      $gallery = $('.gallery'),
      $arrow_icn = $('.arrow-icn'),
      start_image_index = $gallery.attr('data-start-image-index') - 1,
      page_padding = parseInt($('.wrapper.content').css('padding-top')),
      gallery_height_reduction = page_padding + 40,
      window_height = $window.height();

    var updateURL = function (e) {
      var path =
        window.location.pathname
        .split('/').filter(function (n) { return n != ""; })
        .slice(0, 3)
        .join('/') +'/'+
        (e.currentSlide+1);

      history.replaceState(null, null, path);
    };

    /*
     * Used by: $window.resize
     */
    var setBodyGalleryHeight = function () {
      window_height = $window.height();
      $body.css('min-height', window_height + window_height);
      $gallery.css('height', window_height - gallery_height_reduction);
    };

    /*
     * Used by: $('.work-details__toggle-desc-btn').click
     *
     * Determine wether to scroll down or up before you do so. */
    var toggleDescription = function () {
      var value_to_scroll = window_height;
      if ($hustensaft.scrollTop() > window_height / 2) {
        value_to_scroll = 0;
      }
      $scrollBody.animate({scrollTop: value_to_scroll});
    };

    /*
     * Used by: $window.scroll
     *
     * Update appearance of arrow by scroll pos. */
    var updateArrowIcnByScrollPos = function () {
      if ($hustensaft.scrollTop() > window_height / 2) {
        $arrow_icn
          .addClass('arrow-icn--up')
          .removeClass('arrow-icn--down');
      } else {
        $arrow_icn
          .addClass('arrow-icn--down')
          .removeClass('arrow-icn--up');
      }
    };

    var getCurrent$Slide = function (e) {
      var currentSlide = e.slides[e.currentSlide];
      return $(currentSlide);
    };

    var isVideo = function ($slide) {
      return $slide.hasClass('gallery__item--video');
    };

    var
      stopVideo,
      playVideo;

    if (config.fade_audio) {
      stopVideo = function (video) {
        fadeOutAudio(video, function (video) {
          video.pause();
          video.currentTime = 0;
        });
      };

      playVideo = function (video) {
        video.play();
        fadeInAudio(video);
      };
    } else {
      stopVideo = function (video) {
        video.pause();
        video.currentTime = 0;
      };

      playVideo = function (video) {
        video.play();
      };
    }

    // Called before sliding.
    // Targets the current slide that is slided from.
    var resetVideo = function ($slide) {
      var video = $slide.find('video')[0];
      if (video.currentTime > 0) {
        stopVideo(video);
			}
    };

    // Called after sliding and when gallery has initialized.
    // Targets the slide that was slided to, resp. the start slide.
    var toggleVideo = function ($slide) {
      var video = $slide.find('video')[0];
      if (video.currentTime > 0 &&
				!(video.paused || video.ended)) {
        video.pause();
			} else {
        playVideo(video);
			}
    };

    // TODO: This should rather be done in a OOP way. It works but
    // it's horrible.
    var
      ivFadeOutAudio,
      ivFadeInAudio,
      clearIvFadeOutAudio,
      clearIvFadeInAudio,
      ivFadeOutAudio_delay = (config.fade_out_audio_duration * 0.05) - 3,
      ivFadeInAudio_delay = config.fade_in_audio_duration * 0.05,
      is_ivFadeOutAudio_running = false,
      video_using_ivFadeOutAudio = {};

    var fadeOutAudio = function (video, callback) {
      clearInterval(ivFadeInAudio);
      clearTimeout(clearIvFadeOutAudio);
      clearIvFadeOutAudio = setTimeout(function () {
        clearInterval(ivFadeOutAudio);
      }, config.fade_out_audio_duration);

      if (is_ivFadeOutAudio_running) {
        clearInterval(ivFadeOutAudio);
        is_ivFadeOutAudio_running = false;
        video_using_ivFadeOutAudio.pause();
        video_using_ivFadeOutAudio.currentTime = 0;
      }

      is_ivFadeOutAudio_running = true;
      video_using_ivFadeOutAudio = video;

      ivFadeOutAudio = setInterval(function() {
        if (video.volume > 0.05) {
          video.volume -= 0.05;
        } else {
          video.volume = 0;
          clearInterval(ivFadeOutAudio);
          is_ivFadeOutAudio_running = false;
          callback(video);
        }
      }, ivFadeOutAudio_delay);
    };

    var fadeInAudio = function (video) {
      clearTimeout(clearIvFadeInAudio);
      clearIvFadeInAudio = setTimeout(function () {
        clearInterval(ivFadeInAudio);
      }, config.fade_in_audio_duration);

      video.volume = 0;

      ivFadeInAudio = setInterval(function() {
        if (video.volume < 0.95) {
          video.volume += 0.05;
        } else {
          video.volume = 1;
          clearInterval(ivFadeInAudio);
        }
      }, ivFadeInAudio_delay);
    };

    // Also called when gallery has initialized.
    var afterSlide = function (e) {
      var $currentSlide = getCurrent$Slide(e);
      if (isVideo($currentSlide)) {
        toggleVideo($currentSlide);
      }
    };

    var beforeSlide = function (e) {
      // stop all videos, just because...
      $('video').each(function (_, x) {
        x.pause();
      });
      var $currentSlide = getCurrent$Slide(e);
      if (isVideo($currentSlide)) {
        resetVideo($currentSlide);
      }
    };

    $gallery.flexslider({
      namespace: "gallery-",
      slideshow: false,
      animation: "slide",
      // NOTE: If problems appear related to 3d-translated video elements,
      // consider using 'video: true' and/or turn off 'useCSS' in favor of
      // jQuery animations, which seem to be a bit choppy, though.
      // https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties#video
      // video: true,
      useCSS: true,
      touch: true,
      controlNav: false,
      startAt: start_image_index,
      animationSpeed: 600,
      keyboard: false,
      prevText: "",
      nextText: "",
      //customDirectionNav:,

      start: function (e) {
        if (touch)
          $('.gallery-direction-nav').css('display', 'none');
        $gallery.removeClass('js-hide');
        afterSlide(e);
      },
      before: beforeSlide,
      after: function (e) {
        afterSlide(e);
        if (history.replaceState) {
          updateURL(e);
        }
      }
    });

    setBodyGalleryHeight();

    /*
     * Handler
     */
    $window
      .resize(setBodyGalleryHeight)
      .scroll(updateArrowIcnByScrollPos);

    $('.work-details__toggle-desc-btn').click(toggleDescription);
    // $('.work-details__close-btn').click(function () {
    //   history.back();
    // });
  });
})(window, document, jQuery);
