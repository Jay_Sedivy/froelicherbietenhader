<html>
    <head>
        <title>On the fly watermark</title>
        <style>
            @import url(//fonts.googleapis.com/css?family=Raleway:400,300,700);

            * {
                padding: 0px;
                margin: 0px;
            }

            body {
                background-color: #222;
                font-family: 'Raleway', sans-serif;
                font-weight: 400;
                color: #EEE;
                text-align: center;
            }

            h1 {
                padding: 15px;
                font-weight: 700;
                text-align: center;
                background-color: #000;
                background-color: rgba(0,0,0,0.2);
            }

            .main {
                width: 90%;
                min-width: 350px;
                max-width: 1035px;
                margin: 0px auto;
                text-align: left;
                padding: 25px;
            }

            .ex {
                border: #111 solid 1px;
                background: #2a2a2a;
                padding: 10px;
                margin-top: 25px;
            }

            .ex h2 {
                background: #1a1a1a;
                margin: -10px;
                padding: 10px;
            }

            .ex img {
                margin: 20px;
                border: #FFF solid 3px;
                vertical-align: middle;
                box-shadow: 0px 10px 10px 5px rgba(1, 1, 1, 0.5);
                -webkit-transition: opacity 2s;
            }

            .ex img:hover {
                opacity: 0.5;
            }

            .code {
                margin: 30px 25px 25px 25px;
                background: #EEE;
                color: #111;
                font-weight: 300;
                padding: 10px;
                border: #111 solid 1px;
            }
        </style>
    </head>
    <body>
        <h1>On the fly watermark - Demo</h1>
        <div class="main">
            <?php
            for ($i = 0; $i < 10; $i++) {
                echo "<div class='ex'><h2>Example $i</h2>";
                ?>
                <div class="code">
                    <?php
                    if ($i > 0) {
                        $file = "img$i/watermark.php";
                        $lines = file($file);

                        $k = 2;
                        while(!strstr($lines[$k-1],");")) {
                            echo str_replace("©", "&copy;", $lines[$k]) . "<br />";
                            $k++;
                        }
                    } else {
                        echo "These are the original pictures";
                    }
                    ?>
                </div>
                <?php
                for ($j = 1; $j < 5; $j++) {
                    echo "<img src='img$i/$j.jpg' />";
                }
                echo "</div>";
            }
            ?>
        </div>
    </body>
</html>