<?php

$settings = array(
    "position" => array('horizontal' => 'right', 'vertical' => 'bottom'),
    "margin" => 5,
    "content" => "watermark.png",
    "type" => "image"
);

///////////////////////////// DO NOT EDIT BELOW THIS LINE //////////////////////////////////////
// Checks if this file is called directly, if so cancel the script
if (!isset($_GET["file"])) {
    echo "Don't call this file directly";
    exit(0);
}

// Define the image functions for every image format
// Supported formats are: png, jpe, jpeg, jpg, gif, bmp
$image_functions = array(
    'png' => array('mime' => 'image/png', 'in' => 'imagecreatefrompng', 'out' => 'imagepng'),
    'jpe' => array('mime' => 'image/jpeg', 'in' => 'imagecreatefromjpeg', 'out' => 'imagejpeg'),
    'jpeg' => array('mime' => 'image/jpeg', 'in' => 'imagecreatefromjpeg', 'out' => 'imagejpeg'),
    'jpg' => array('mime' => 'image/jpeg', 'in' => 'imagecreatefromjpeg', 'out' => 'imagejpeg'),
    'gif' => array('mime' => 'image/gif', 'in' => 'imagecreatefromgif', 'out' => 'imagegif'),
    'bmp' => array('mime' => 'image/bmp', 'in' => 'imagecreatefromwbmp', 'out' => 'imagewbmp')
);

// Get the requested file name
$filename = $_GET["file"];

// Get the extensions, $filename_exploded variable to work in strict-mode
$filename_exploded = explode('.', $filename);
$ext = strtolower(array_pop($filename_exploded));

// Check if the extension is supported, if not exit
if (!array_key_exists($ext, $image_functions)) {
    die("The extention '$ext' is not supported at the moment!");
}

// Now know the image is supported we can safely support it
$image = $image_functions[$ext]["in"]($filename);

// Tell the server that the output of this script has the same mime as the original image
header("Content-Type: " . $image_functions[$ext]["mime"]);

// Load the dimension (width, height) from the image
$dimensions = getimagesize($filename);
$width = $dimensions[0];
$height = $dimensions[1];

// Get the size of the watermark
if ($settings["type"] != "image") {
    $font_dimensions = imagettfbbox($settings["font"]["size"], 0, $settings["font"]["file"], $settings["content"]);
} else {
    $image_watermark_dimensions = getimagesize($settings["content"]);
}

// Calculate the actual size of the watermark
$watermark_width = ($settings["type"] != "image") ? abs($font_dimensions[2] - $font_dimensions[0]) : $image_watermark_dimensions[0];
$watermark_height = ($settings["type"] != "image") ? abs($font_dimensions[3] - $font_dimensions[5]) : $image_watermark_dimensions[1];

// Calculate the x value based on the settings
$x;
switch ($settings["position"]["horizontal"]) {
    case "left":
        $x = $settings["margin"];
        break;
    case "center":
        $x = ($width - $watermark_width) / 2;
        break;
    case "right":
    default:
        $x = $width - $settings["margin"] - $watermark_width;
}

// Calculate the y value based on the settings
$y;
switch ($settings["position"]["vertical"]) {
    case "top":
        $y = $settings["margin"] + ($settings["type"] != "image" ? $settings["font"]["size"] : $watermark_height);
        break;
    case "center":
        $y = ($height + $watermark_height) / 2;
        break;
    case "bottom":
    default:
        $y = $height - $settings["margin"];
}

// Copy the watermark on the new image
if ($settings["type"] != "image") {
    // Place the text on the image
    $text_color = imagecolorallocatealpha($image, $settings["color"]["r"], $settings["color"]["g"], $settings["color"]["b"], (1 - $settings["color"]["alpha"]) * 127);
    imagettftext($image, $settings["font"]["size"], 0, $x, $y, $text_color, $settings["font"]["file"], $settings["content"]);
} else {
    // Get the watermark image filename + extension (strict mode)
    $watermark_path_exploded = explode('.', $settings["content"]);
    $watermark_ext = strtolower(array_pop($watermark_path_exploded));

    // Check if the image has the appropriate extension
    if (array_key_exists($watermark_ext, $image_functions)) {
        // Load the watermark image using the appropriate function
        $image_watermark = $image_functions[$watermark_ext]["in"]($settings["content"]);

        // Place the watermark on the image
        imagecopy($image, $image_watermark, $x, $y - $watermark_height, 0, 0, $watermark_width, $watermark_height);

        // Free resources
        imagedestroy($image_watermark);
    } else {
        die("The extention '$ext' is not supported at the moment!");
    }
}

// Output the image using the appropriate function for the imagetype
$image_functions[$ext]["out"]($image);

// Free the memory used by the image
imagedestroy($image);