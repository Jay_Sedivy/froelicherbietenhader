<?php
  include('../admin/runtime.php');
  
  /* NOTE: The content of this page is not visible to the user. It acts like
   * a DB for 'index.php'. For an explanation, check the note on that page. */
?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>

    <?php perch_content('Work'); ?>

  </body>
</html>
