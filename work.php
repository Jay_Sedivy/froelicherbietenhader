<?php
  include('admin/runtime.php');
  include('admin/lang.php');

  $path_exploded = preg_split(
    '~/~', "$_SERVER[REQUEST_URI]", NULL, PREG_SPLIT_NO_EMPTY
  );

  // Path array (0:'work', 1:'[slug by title]', 2:'[start image index]')
  $work_uid = $path_exploded[1];
  $title = $path_exploded[2];
  $start_image_index = !$path_exploded[3] ? '1' : $path_exploded[3];

  // Var is used in template 'work_details_content' in element
  // .flexslider[data-start-image-index]
  PerchSystem::set_var('start_image_index', $start_image_index);

  perch_layout('work.top', array(
    'id'=>'work',
  ));

  perch_layout('work.header.wo-nav');

  perch_content_custom('Work', array(
    'page' => '/work_content/work_content.php',
    'template' => 'work_details_content.html',
    'filter' => '_id',
    'match' => 'eq',
    'value' => $work_uid,
    'count' => 1,
  ));

  perch_layout('work.bottom');

  PerchSystem::unset_var('start_image_index');
?>
