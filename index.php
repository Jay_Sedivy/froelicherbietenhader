<?php
  include('admin/runtime.php');
  include('admin/lang.php');

  PerchSystem::set_var('lang', $_SESSION['lang']);
  perch_layout('base.top', array(
    'id'=>'index',
  ));
  perch_layout('base.header');
?>

<div class="wrapper content">

<?php
  /* NOTE: This content is written at runtime. Because of the HTML
   * structure and the fact that perch (not runway) content is bound to
   * pages, i could not find a better solution for this. I might
   * be wrong though. I think, performance is okay for now.
   *
   * Both of these custom regions are wrapped by an element with
   * class 'grid--work'. The content is taken from 'work_content.php' which
   * is used like a content DB and where top and bottom content is part
   * of the same perch_content region. By this, the editor of 'Work' is
   * able to edit the content on the same cms page. Again, i might be
   * wrong and there might be a better solution. Alternatively, i could
   * have still used perch_content and wrapped the output with javascript
   * which was IMO slightly worse than this. Eventually, it was just a
   * matter of choice between php and javascript. */

  // TODO: It might be a good idea to constrain the queries by using a filter
  // for top content and one for bottom content.
  perch_content_custom('Work', array(
    'page'=>'/work_content/work_content.php',
    'template'=>'work_top_content.html',
  ));

  perch_content_custom('Work', array(
    'page'=>'/work_content/work_content.php',
    'template'=>'work_bottom_content.html',
  ));
?>

</div>

<?php perch_layout('index.bottom'); ?>
